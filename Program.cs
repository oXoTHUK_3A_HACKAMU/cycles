﻿using System;
using System.Collections.Generic;

namespace cycle
{
    class Program
    {
        static void Main(string[] args)
        {
            Task6();
        }


        public static void Task2()
        {
            int a = 0;
            string str = "";
            for (int i = 1; i < 5; i++)
            {
                while (a < i)
                {
                    str += "0";
                    a++;
                    Console.WriteLine(str);
                }
            }
        }
        public static void Task3()
        {
            string Text1 = "";
            for (int i = 0; i < 5; i++)
            {
                if (i % 2 == 0)
                {
                    Text1 += "1";
                }
                else
                {
                    Text1 += "0";
                }
                Console.WriteLine(Text1);
            }
        }
        public static void Task4()
        {
            Random rnd = new Random();
            int appraisal = rnd.Next(0, 100);
            Console.WriteLine(appraisal);
            if (appraisal <= 70)
                Console.WriteLine("удовлетворительно");
            if(appraisal >= 71 && appraisal <= 80)
                Console.WriteLine("хорошо");
            if(appraisal >= 81 && appraisal <= 100)
                Console.WriteLine("отлично");
        }
        public static void Task5()
        {
            Console.Write("Введите число: ");
            int chislo_1 = Convert.ToInt32(Console.ReadLine());
            chislo_1 = ((chislo_1 - (chislo_1 % 1000)) / 10 + chislo_1 % 100);
            Console.WriteLine(chislo_1);
        }
        public static void Task6()
        {
            string a1 = "";
            string b2 = "";
            Random rnd = new Random();
            int[] x = new int[4];
            int[] y = new int[4];
            List<int> number = new List<int>();
            for (int i = 0; i < x.Length; i++)
            {
                x[i] = rnd.Next(0,9);
                a1 += x[i].ToString();
                for (int j = 0; j < y.Length; j++)
                {
                    y[j] = rnd.Next(0, 9);
                    b2 += y[j].ToString();
                    if(y[j] == x[i])
                    {
                        if (!number.Contains(x[i]))
                        {
                            number.Add(x[i]);
                            Console.WriteLine("Есть : " + x[i]);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Нету");
                    }
                }
            }
            Console.WriteLine(a1);
            Console.WriteLine(b2);
        }
    }
}
